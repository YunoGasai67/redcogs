from redbot.core import commands

class FirstCog(commands.Cog):
    """My custom cog"""

    @commands.command()
    async def FirstCog(self, ctx):
        """This does stuff!"""
        # Your code will go here
        await ctx.send("This is my first Red Cog, woo")